package Combinations

interface ICombination {
    val name : String
    fun calculate(valuesCount: Map<Int,Int>) : Int
}