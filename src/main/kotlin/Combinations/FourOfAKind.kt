package Combinations

class FourOfAKind {
    companion object FourOfAKind : ICombination {
        override val name: String = "Four Of A Kind"
        override fun calculate(valuesCount: Map<Int, Int>): Int {
            for (numberOfDots in 1..6) {
                if (valuesCount[numberOfDots]!! >= 4) {
                    return numberOfDots * 4 + 40
                }
            }
            return 0
        }
    }
}
