package Combinations

class LargeStraight {
    companion object LargeStraight : ICombination {
        override val name: String = "Large Straight"
        override fun calculate(valuesCount: Map<Int, Int>): Int {
            if (valuesCount[2]!! > 0 &&
                valuesCount[3]!! > 0 &&
                valuesCount[4]!! > 0 &&
                valuesCount[5]!! > 0
            ) {

                if (valuesCount[6]!! > 0) {
                    return 40
                } else if (valuesCount[1]!! > 0) {
                    return 30
                }
            }
            return 0
        }
    }
}
