package Combinations

class Yahtzee {
    companion object Yahtzee : ICombination {
        override val name: String = "Combinations.Yahtzee"
        override fun calculate(valuesCount: Map<Int, Int>): Int {
            for (numberOfDots in 1..6) {
                if (valuesCount[numberOfDots]!! >= 5) {
                    return numberOfDots * 5 + 50
                }
            }
            return 0
        }
    }
}
