package Combinations

class OneToSix {
    companion object OneToSix : ICombination {
        override val name: String = "One to Six"
        override fun calculate(valuesCount: Map<Int, Int>): Int {
            var max = 0
            for ((k, v) in valuesCount) {
                if (k * v > max) {
                    max = k * v
                }
            }
            return max
        }
    }
}
