package Combinations

class FullHouse {
    companion object FullHouse : ICombination {
        override val name: String = "Full House"

        override fun calculate(valuesCount: Map<Int, Int>): Int {
            var tempValuesCount: MutableMap<Int, Int> = valuesCount.toMutableMap()
            var threeOf: Int = 0

            for (numberOfDots in 6 downTo 1) {
                if (tempValuesCount[numberOfDots]!! >= 3) {
                    tempValuesCount[numberOfDots] = tempValuesCount[numberOfDots]!! - 3
                    threeOf = numberOfDots
                    for (numberOfDots in 6 downTo 1) {
                        if (tempValuesCount[numberOfDots]!! >= 2) {
                            return threeOf * 3 + 2 * numberOfDots + 30
                        }
                    }
                }
            }
            return 0
        }
    }
}
