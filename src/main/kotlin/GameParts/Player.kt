package GameParts

class Player(var name: String = "") {
    val dice: Array<Die> = Array(6) { Die() }
    var currentScore: Int = 0

    fun rollDice() {
        dice.forEach { die -> die.roll() }
    }
    fun lockDice(ordinals: List<Int>) {
        ordinals.forEach { ordinal -> dice[ordinal - 1].lock() }
    }

    fun unlockAllDice() {
        dice.forEach { die -> die.unlock() }
    }
}
