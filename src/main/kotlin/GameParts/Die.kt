package GameParts

class Die {
    var rolledNumber: Int = 0
    var isLocked: Boolean = false

    override fun toString(): String {
        return rolledNumber.toString()
    }
    fun roll() {
        if (!isLocked) {
            rolledNumber = (1..6).random()
        }
    }

    fun lock() {
        isLocked = true
    }

    fun unlock() {
        isLocked = false
    }
}
