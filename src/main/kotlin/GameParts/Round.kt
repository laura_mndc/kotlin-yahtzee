package GameParts

class Round(private val roundNumber: Int, private val players: MutableList<Player>) {

    fun start() {
        println("ROUND $roundNumber")
        for (playerIndex in 0 until players.size) {
            val turn = Turn(players[playerIndex])
            turn.start()
        }
    }
}
