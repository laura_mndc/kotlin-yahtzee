package GameParts

import Combinations.CombinationScore

class Roll(private val player: Player) {

    lateinit var maxOptionInRoll: CombinationScore

    fun start() {
        player.rollDice()
        println("You rolled: ${player.dice.joinToString()}")
        maxOptionInRoll = ResultCalculator.getBestOption(player.dice)
        println(
            "Current best option is worth ${maxOptionInRoll.score} points with ${maxOptionInRoll.combination.name}."
        )
    }

    fun getScore(): Int {
        return maxOptionInRoll.score
    }
}
