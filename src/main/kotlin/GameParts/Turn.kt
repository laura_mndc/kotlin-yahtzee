package GameParts

class Turn(private val player: Player) {

    fun start() {
        println("")
        player.unlockAllDice()
        println("GameParts.Player ${player.name.uppercase()}")
        var playerRoundScore: Int = 0
        for (numberOfRoll in 1..3) {
            val roll = Roll(player)
            roll.start()
            playerRoundScore = roll.getScore()

            if (numberOfRoll < 3) {
                if (!checkWillRollAgain()) {
                    break
                }
                lockDice()
            }
        }

        player.currentScore += playerRoundScore
        println("Current score of player ${player.name} is ${player.currentScore}.\n")
    }

    private fun checkWillRollAgain(): Boolean {
        print("Do you want to roll again? (y/n) ")
        val willRollAgain = readLine()
        if (willRollAgain?.lowercase() == "n") {
            return false
        }
        return true
    }

    private fun lockDice() {
        print(
            "Enter ordinal numbers of dice you wish to lock, or -1 if you wish to roll all of them again: "
        )
        val diceOrdinalsToLock: List<Int> = readLine()!!.split(' ').map { die -> die.toInt() }
        if (diceOrdinalsToLock[0] != -1) {
            player.lockDice(diceOrdinalsToLock)
        }
    }
}
