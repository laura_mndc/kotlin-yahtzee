package GameParts

class YahtzeeGameEngine(private val numberOfPlayers: Int = 1, private val numberOfRounds: Int = 1) {

    private val players: MutableList<Player> = mutableListOf()

    fun enterPlayerNames() {
        for (i in 1..numberOfPlayers) {
            print("Enter name of player ${i}: ")
            players.add(Player(readLine()!!))
        }
    }

    private fun declareWinner() {
        var winningScore = players.maxOf { player -> player.currentScore }
        var winners = players.filter { player -> player.currentScore == winningScore }
        if (winners.size == 1) {
            println("Winner is ${winners[0].name} with a score of $winningScore")
        } else {
            println(
                "Winners are ${winners.joinToString { player -> player.name }} with a score of $winningScore"
            )
        }
    }

    fun startNewGame() {
        for (roundNumber in 1..numberOfRounds) {
            val round = Round(roundNumber, players)
            round.start()
        }
        declareWinner()
    }
}
