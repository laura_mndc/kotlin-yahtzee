package GameParts

import Combinations.*

class ResultCalculator() {
    companion object {
        var combinations: Array<ICombination> =
            arrayOf(OneToSix, LargeStraight, FourOfAKind, FullHouse, Yahtzee)

        fun getBestOption(dice: Array<Die>): CombinationScore {
            var valuesCount: Map<Int, Int> =
                mapOf(
                    1 to dice.count { it.rolledNumber == 1 },
                    2 to dice.count { it.rolledNumber == 2 },
                    3 to dice.count { it.rolledNumber == 3 },
                    4 to dice.count { it.rolledNumber == 4 },
                    5 to dice.count { it.rolledNumber == 5 },
                    6 to dice.count { it.rolledNumber == 6 }
                )

            var maxCombination = CombinationScore(OneToSix, 0)
            for (combination in combinations) {
                var currentResult = combination.calculate(valuesCount)
                if (currentResult > maxCombination.score) {
                    maxCombination.score = currentResult
                    maxCombination.combination = combination
                }
            }
            return maxCombination
        }
    }
}
