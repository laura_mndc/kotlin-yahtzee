import GameParts.YahtzeeGameEngine

fun main(args: Array<String>) {
    print("Enter number of rounds: ")
    val numberOfRounds: Int = readLine()!!.toInt()

    print("Enter number of players: ")
    val numberOfPlayers: Int = readLine()!!.toInt()

    val gameEngine = YahtzeeGameEngine(numberOfPlayers, numberOfRounds)
    gameEngine.enterPlayerNames()
    gameEngine.startNewGame()
}
